//
//  URLUtils.m
//  iOSMDMAgent


#import "URLUtils.h"
#import <UIKit/UIKit.h>

@implementation URLUtils

+ (NSDictionary *)readEndpoints {
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:ENDPOINT_FILE_NAME ofType:EXTENSION];
    return [NSDictionary dictionaryWithContentsOfFile:plistPath];
}

+ (NSString *)getServerURL {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:SERVER_URL];
}

+ (NSString *)getContextURL {
    return [[URLUtils readEndpoints] objectForKey:CONTEXT_URI];
}

+ (NSString *)getAPIPort {
    return [[URLUtils readEndpoints] objectForKey:API_PORT];
}

+ (NSString *)getEnrollmentURLFromPlist {
    return [[URLUtils readEndpoints] objectForKey:ENROLLMENT_URL];
}

+ (NSString *)getEnrollmentType {
    return [[URLUtils readEndpoints] objectForKey:AGENT_BASED_ENROLLMENT];
}

+ (NSString *)getServerURLFromPlist {
    return [[URLUtils readEndpoints] objectForKey:SERVER_URL];
}

+ (NSString *)getEnrolmentPort {
    return [[URLUtils readEndpoints] objectForKey:ENROLMENT_PORT];
}

+ (NSString *)getSavedEnrollmentURL {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults objectForKey:ENROLLMENT_URL];
}

+ (void)saveEnrollmentURL:(NSString *)enrollURL {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:enrollURL forKey:ENROLLMENT_URL];
    [userDefaults synchronize];
}

+ (void)saveServerURL:(NSString *)serverURL {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:serverURL forKey:SERVER_URL];
    [userDefaults synchronize];
}

+ (NSString *)getTokenPublishURL {
    return [NSString stringWithFormat:@"%@:%@%@%@", [URLUtils getServerURL], [URLUtils getAPIPort], [URLUtils getContextURL], [[URLUtils readEndpoints] objectForKey:TOKEN_PUBLISH_URI]];
}

+ (NSString *)getOperationURL {
    return [NSString stringWithFormat:@"%@:%@%@%@", [URLUtils getServerURL], [URLUtils getAPIPort], [URLUtils getContextURL], [[URLUtils readEndpoints] objectForKey:OPERATION_URI]];
}

+ (NSString *)getUnenrollURL {
    return [NSString stringWithFormat:@"%@:%@%@%@", [URLUtils getServerURL], [URLUtils getAPIPort], [URLUtils getContextURL], [[URLUtils readEndpoints] objectForKey:UNENROLLMENT_PATH]];
}

+ (NSString *)getRefreshTokenURL{
    return [NSString stringWithFormat:@"%@:%@%@", [URLUtils getServerURL], [URLUtils getAPIPort], [[URLUtils readEndpoints] objectForKey:REFRESH_TOKEN_URI]];
}

+ (NSString *)getEnrollmentURL {
    return [NSString stringWithFormat:@"%@:%@%@", [URLUtils getSavedEnrollmentURL], [URLUtils getEnrolmentPort], [[URLUtils readEndpoints] objectForKey:ENROLLMENT_URI]];
}

+ (NSString *)getCaDownloadURL {
    return [NSString stringWithFormat:@"%@:%@%@", [URLUtils getSavedEnrollmentURL], [URLUtils getEnrolmentPort], [[URLUtils readEndpoints] objectForKey:CA_DOWNLOAD_PATH]];
}

+ (NSString *)getTokenRefreshURL {
    return [NSString stringWithFormat:@"%@:%@%@", [URLUtils getSavedEnrollmentURL], [URLUtils getEnrolmentPort], [[URLUtils readEndpoints] objectForKey:TOKEN_REFRESH_URI]];
}

+ (NSString *)getEffectivePolicyURL {
    return [NSString stringWithFormat:@"%@:%@%@%@", [URLUtils getServerURL], [URLUtils getAPIPort], [URLUtils getContextURL], [[URLUtils readEndpoints] objectForKey:EFFECTIVE_POLICY_PATH]];
}

+ (NSString *)getLocationPublishURL {
    return [NSString stringWithFormat:@"%@:%@%@%@", [URLUtils getServerURL], [URLUtils getAPIPort], [URLUtils getContextURL], [[URLUtils readEndpoints] objectForKey:LOCATION_PUBLISH_URI]];
}

+ (NSString *)getAuthenticationURL {
    return [NSString stringWithFormat:@"%@:%@%@%@",  [URLUtils getServerURL], [URLUtils getAPIPort], [URLUtils getContextURL], [[URLUtils readEndpoints] objectForKey:AUTH_PATH]];
}

+ (NSString *)getLicenseURL {
    return [NSString stringWithFormat:@"%@:%@%@", [URLUtils getSavedEnrollmentURL], [URLUtils getEnrolmentPort], [[URLUtils readEndpoints] objectForKey:LICENSE_PATH]];
}

+ (NSString *)getEnrollURL:(NSString *)tenantDomain username:(NSString *)token {
    NSArray *osVersion = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[osVersion objectAtIndex:0] intValue] >= 10) {
        return [NSString stringWithFormat:@"%@:%@%@?tenantdomain=%@&token=%@&higer=true", [URLUtils getSavedEnrollmentURL], [URLUtils getEnrolmentPort], [[URLUtils readEndpoints] objectForKey:MOBICONFIG_PATH], tenantDomain, token];
    }
    return [NSString stringWithFormat:@"%@:%@%@?tenantdomain=%@&token=%@", [URLUtils getSavedEnrollmentURL], [URLUtils getEnrolmentPort], [[URLUtils readEndpoints] objectForKey:MOBICONFIG_PATH], tenantDomain, token];
}

+ (NSString *)getIsEnrolledURL {
    return [NSString stringWithFormat:@"%@:%@%@%@", [URLUtils getSavedEnrollmentURL], [URLUtils getEnrolmentPort],[URLUtils getContextURL], [[URLUtils readEndpoints] objectForKey:IS_ENROLLED_PATH]];
}

@end
