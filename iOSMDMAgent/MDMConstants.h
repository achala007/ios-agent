//
//  MDMConstants.h
//  iOSMDMAgent
//
//

#import <Foundation/Foundation.h>

@interface MDMConstants : NSObject

extern NSString *const OK_BUTTON_TEXT;
extern NSString *const CANCEL_BUTTON_TEXT;
extern NSString *const EMPTY_URL;
extern NSString *const EMPTY_URL_MESSAGE;
extern NSString *const AUTH_ERROR_TITLE;
extern NSString *const AUTH_ERROR_MESSAGE;
extern NSString *const INVALID_SERVER_URL;
extern NSString *const INVALID_SERVER_URL_MESSAGE;
extern NSString *const UNREGISTER_ERROR;
extern NSString *const UNREGISTER_ERROR_MESSAGE;

extern float const HTTP_REQUEST_TIME;
extern int HTTP_OK;
extern int HTTP_CREATED;
extern int HTTP_BAD_REQUEST;
extern NSString *const API_PORT;
extern NSString *const ENROLMENT_PORT;
extern NSString *const ENDPOINT_FILE_NAME;
extern NSString *const EXTENSION;
extern NSString *const TOKEN_PUBLISH_URI;
extern NSString *const ENROLLMENT_URI;
extern NSString *const SERVER_URL;
extern NSString *const CONTEXT_URI;
extern NSString *const TOKEN;
extern NSString *const GET;
extern NSString *const POST;
extern NSString *const PUT;
extern NSString *const ACCEPT;
extern NSString *const CONTENT_TYPE;
extern NSString *const APPLICATION_JSON;
extern NSString *const LATITIUDE;
extern NSString *const LONGITUDE;
extern NSString *const UNENROLLMENT_PATH;
extern NSString *const AUTHORIZATION_BEARER;
extern NSString *const AUTHORIZATION_BASIC;
extern NSString *const AUTHORIZATION;
extern NSString *const REFRESH_TOKEN_URI;
extern NSString *const REFRESH_TOKEN_LABEL;
extern NSString *const GRANT_TYPE;
extern NSString *const GRANT_TYPE_VALUE;
extern NSString *const FORM_ENCODED;
extern NSString *const OPERATION_URI;
extern NSString *const OPERATION_ID_RESPOSNE;
extern NSString *const STATUS;
extern int OAUTH_FAIL_CODE;
extern NSString *const ENROLLMENT_URL;
extern NSString *const EFFECTIVE_POLICY_PATH;
extern NSString *const TOKEN_REFRESH_URI;
extern NSString *const TENANT_NAME;
extern NSString *const USERNAME;
extern NSString *const PASSWORD;
extern NSString *const AGENT_BASED_ENROLLMENT;
extern NSString *const CA_DOWNLOAD_PATH;
extern NSString *const AUTH_PATH;
extern NSString *const LICENSE_PATH;
extern NSString *const ENROLL_PATH;
extern NSString *const IS_ENROLLED_PATH;
extern NSString *const MOBICONFIG_PATH;
extern NSString *const AUTO_ENROLLMENT;
extern NSString *const AUTO_ENROLLMENT_STATUS_PATH;
extern NSString *const AUTO_ENROLLMENT_COMPLETED;
extern NSString *const IS_AGENT_AVAILABLE;
extern NSString *const LOCATION_PUBLISH_URI;

extern int LOCATION_DISTANCE_FILTER;
extern NSString *const UDID;
extern NSString *const APS;
extern NSString *const EXTRA;
extern NSString *const OPERATION;
extern NSString *const SOUND_FILE_NAME;
extern NSString *const SOUND_FILE_EXTENSION;
extern NSString *const ENCLOSING_TAGS;
extern NSString *const TOKEN_KEYCHAIN;
extern NSString *const CLIENT_DETAILS_KEYCHAIN;
extern NSString *const ENROLL_STATUS;
extern NSString *const ENROLLED;
extern NSString *const UNENROLLED;
extern NSString *const OPERATION_ID;
extern NSString *const LOCATION_OPERATION_ID;
extern NSString *const ACCESS_TOKEN;
extern NSString *const REFRESH_TOKEN;
extern NSString *const CLIENT_CREDENTIALS;
extern NSString *const CHALLANGE_TOKEN;
extern NSString *const LICENSE_TEXT;
extern NSString *const TENANT_DOMAIN;
extern NSString *const LOCATION_UPDATED_TIME;
extern NSString *const AUTHENTICATION_FAIL;
extern NSString *const EMPTY_USERNAME;
extern NSString *const EMPTY_PASSWORD;
extern NSString *const ENROLLMENT_SUCCESS;

@end
